# Spring Boot Transaction Management Example

## 1. Project Description

This repository serves as a demonstration of transaction management in a Spring Boot application. The project showcases
fundamental concepts of handling transactions in a database system, providing a clear example for developers to
understand and learn.

## 2. Prerequisites

Before you start, ensure you have the following requirements in place:

- Java Development Kit (JDK) installed
- Git installed
- An Integrated Development Environment (IDE) such as IntelliJ IDEA or Eclipse
- Maven installed (for dependency management)

## 3. Getting Started

1. Clone the Repository:

```
git clone https://gitlab.com/techtalk5782345/transaction-management-example.git
```

2. Navigate to the project directory:

```
cd transaction-management-example
```

3. Run the Spring Boot Application

``` 
./mvnw spring-boot:run
```

## Access the H2 In-Memory Database

Navigate to the H2 console for database interactions:

URL: ``http://localhost:8080/h2-console``
Use the credentials specified in ``application.properties``
Explore and experiment with transactional behavior by transferring money between accounts.

## Project Structure

- ``src/main/java/com/techtalk/transaction``: Contains the AccountController and Service classes