package com.techtalk.transaction.controller;

import com.techtalk.transaction.model.Account;
import com.techtalk.transaction.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// Controller class that handles the HTTP requests and responses
@RestController
@RequestMapping("/api/accounts")
public class AccountController {

    @Autowired
    private AccountService accountService;

    // GET method that returns all accounts
    @GetMapping
    public List<Account> getAllAccounts() {
        return accountService.getAllAccounts();
    }

    // POST method that transfers money between accounts
    @PostMapping("/transfer")
    public ResponseEntity<String> transferMoney(@RequestParam String from, @RequestParam String to, @RequestParam Double amount) {
        try {
            // invoke the transactional method
            accountService.transferMoney(from, to, amount);
            // return a success message
            return ResponseEntity.ok("Money transferred successfully");
        } catch (Exception e) {
            // return an error message
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}