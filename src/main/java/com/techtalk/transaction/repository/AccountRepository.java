package com.techtalk.transaction.repository;

import com.techtalk.transaction.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

// Repository interface that extends JpaRepository
public interface AccountRepository extends JpaRepository<Account, Long> {

    // custom query method to find account by name
    Account findByName(String name);
}