package com.techtalk.transaction.service;

import com.techtalk.transaction.model.Account;
import com.techtalk.transaction.repository.AccountRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    // transactional method that transfers money from one account to another
    @Transactional
    public void transferMoney(String from, String to, Double amount) {
        // find the accounts by name
        Account fromAccount = accountRepository.findByName(from);
        Account toAccount = accountRepository.findByName(to);

        // check if the accounts exist and have enough balance
        if (fromAccount == null || toAccount == null) {
            throw new IllegalArgumentException("Account not found");
        }
        if (fromAccount.getBalance() < amount) {
            throw new IllegalArgumentException("Insufficient balance");
        }

        // update the balances of the accounts
        fromAccount.setBalance(fromAccount.getBalance() - amount);
        toAccount.setBalance(toAccount.getBalance() + amount);

        // save the accounts to the database
        accountRepository.save(fromAccount);
        accountRepository.save(toAccount);
    }

    // service method that returns all accounts
    public List<Account> getAllAccounts() {
        // use the repository method to find all accounts
        return accountRepository.findAll();
    }
}